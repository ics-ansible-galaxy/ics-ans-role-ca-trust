# ics-ans-role-ca-trust

Ansible role to add Certificate Authority trust.

## Role Variables

```yaml
ca_trust_certificates: []
```

## Example Playbook

```yaml
- hosts: servers
  vars:
    ca_trust_certificates:
      - name: example.com
        certificate: |
          -----BEGIN CERTIFICATE-----
          MIIDazCCAlOgAwIBAgIUFPRH7NJ/LcabNbXMH2jc4FVk3rUwDQYJKoZIhvcNAQEL
          BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
          GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMTAxMjIxNTI5MDhaFw0yMTAy
          MjExNTI5MDhaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
          HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
          AQUAA4IBDwAwggEKAoIBAQDNrWqG+x5hVP/r3BGka/zt3wE3nJAgktk0gPb11p8g
          5j+QjyjXPMQsdGvveBRxjYSCsTlW04B8x4EP7i3F2aH5KrjXiCZpO5QB+y52nSdM
          4IGGha6U4ESq8LCPt02LGqzYk1XRfuLK2N/eEBUXaiB152y2JOaSSgUyK1cER/Jg
          aZnRM8sDrREtd98TYXTyjF10yozsoWABAvMTdKOtXaW5n8iBoYxiNRcImPR0zeRL
          SYXRO2vrwScD1ZE6ik+GHeiSlNvQKdOzhkYw7Jh31RhJiPMRBQvL/+yQH3S4wF2e
          uMXy+NpROrzFE8E6rxvTB7lIXY5n8WNumE21IyROvJ37AgMBAAGjUzBRMB0GA1Ud
          DgQWBBRUoBUAASRSVFM/JYf3hbERIZbyEjAfBgNVHSMEGDAWgBRUoBUAASRSVFM/
          JYf3hbERIZbyEjAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBF
          XWS2eGHfNJmkl/4J2ol1nVjAVOoCq8jiFDf6gu4WWfcu7ZD+acoWsPZV3Tw0PRzV
          gOuw/sflLkUMJInddn2jQOILOH1nlJ5+xORp8ZhOhOk2bF7912GMwUZFMUdsr3zr
          yptzjkh3ircVcCHa5CZt40FRcakvlReo7feSq2A6ob6lRUnzJCRZUyS0DOmdJi4l
          jaocRpqj16VzsG/L3PJGTNeRAOSaE9nEQbAoDyGS4/vpx2iAwny/ewimF35UOPdy
          cOjlj9DPcsDzYtqs+pTKp9Wjmiyc3N6hwuT1IUQfIRSu9mnKe6xK32jTN68MN7Ax
          ktHVarEbE4epDmV4NklB
          -----END CERTIFICATE-----
  roles:
    - role: ics-ans-role-ca-trust
```

## License

BSD 2-clause
